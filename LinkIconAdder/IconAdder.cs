﻿using System;
using System.IO;
using System.Linq;


namespace LinkIconAdder
{
    class IconAdder
    {
        string iconsDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Icons");
        private bool hasHadIconFile = false;
        private bool iconFileExists = false;

        public object Bitmap { get; private set; }

        public void AddIcon(string toFile)
        {
            CheckIfFileHasIconAndItExists(toFile);

            if (!hasHadIconFile || !iconFileExists)
                GetIcon(toFile);
        }

        private void writeIconPath(string file, string iconPath)
        {
            if (!hasHadIconFile)
            {
                using (StreamWriter writer = new StreamWriter(file, true))
                {
                    writer.WriteLine("IconFile=" + iconPath);
                    writer.WriteLine("IconIndex=0");
                }
            }
            else if (hasHadIconFile && !iconFileExists)
            {
                string[] lines = File.ReadAllLines(file);
                File.Delete(file);
                foreach (string line in lines)
                {
                    using (StreamWriter writer = new StreamWriter(file, true))
                    {
                        if (line.ToLower().Contains("iconfile="))
                        {
                            writer.WriteLine("IconFile=" + iconPath);
                        }
                        else
                            writer.WriteLine(line);
                    }
                }
            }

        }

        private void GetIcon(string file)
        {
            string[] lines = File.ReadAllLines(file);

            foreach (string line in lines)
                if (line.Contains("URL="))
                {
                    try
                    {
                        //Uri url = new Uri(line.Split('=').Last());
                        string url = line.Split('/').Skip(2).Take(1).First();

                        Console.WriteLine(url);
                        string pngIconPath = Path.Combine(iconsDir, url + ".png");
                        string iconPath = Path.Combine(iconsDir, url + ".ico");
                        if (File.Exists(pngIconPath))
                        {
                            writeIconPath(file, iconPath);
                            return;
                        }

                        string faviconPng = @"http://www.google.com/s2/favicons?domain=" + url;
                        //string favicon = @"http://www." + url + "/favicon.ico";

                        using (var client = new System.Net.WebClient())
                        {
                            client.DownloadFile(faviconPng, pngIconPath);
                        }

                        //FileStream pngStream = File.OpenRead(pngIconPath);
                        //FileStream iconStream = File.Create(iconPath);
                        //Bitmap bitmap = (Bitmap)Image.FromFile(pngIconPath);
                        //Icon.FromHandle(bitmap.GetHicon()).Save(iconStream);
                        //pngStream.Dispose();
                        //iconStream.Dispose();

                        FileStream iconStream = File.Create(iconPath);
                        FileStream pngStream = File.OpenRead(pngIconPath);
                        
                        PngIconConverter.Convert(pngStream, iconStream, 64);
                        iconStream.Dispose();
                        pngStream.Dispose();
                        writeIconPath(file, iconPath);
                        return;
                    }
                    catch (Exception e)
                    {
                        Console.ForegroundColor = System.ConsoleColor.Red;
                        Console.WriteLine(line);
                        Console.ForegroundColor = System.ConsoleColor.DarkCyan;
                        Console.WriteLine();
                    }
                }
        }

        private void CheckIfFileHasIconAndItExists(string file)
        {
            string[] lines = File.ReadAllLines(file);

            foreach (string line in lines)
                if (line.ToLower().Contains("iconfile="))
                {
                    hasHadIconFile = true;
                    string location = line.Substring(9);
                    if (File.Exists(location))
                        iconFileExists = true;
                    else
                        iconFileExists = false;
                    return;
                }
            hasHadIconFile = false;
        }
    }
}
