﻿using System;

namespace LinkIconAdder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Processer processer = new Processer();
            processer.ProcessFolders(args);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\n      All given .url files are updated");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;

            while (true)
            {
                Console.Write("      Refresh windows explorer to show the icons correctly? (Y/N): ");
                string a = Console.ReadLine();
                if (a.ToLower() == "y")
                {
                    SHChangeNotify(0x8000000, 0x1000, IntPtr.Zero, IntPtr.Zero);
                    Environment.Exit(0);
                }
                else if (a.ToLower() == "n")
                    Environment.Exit(0);
            }
        }
        [System.Runtime.InteropServices.DllImport("Shell32.dll")]
        private static extern int SHChangeNotify(int eventId, int flags, IntPtr item1, IntPtr item2);
    }
}
