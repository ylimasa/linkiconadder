﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LinkIconAdder
{
    class Processer
    {
        List<string> files = new List<string>();

        public void ProcessFolders(string[] foldersAndFiles)
        {
            AddDirItemsToList(foldersAndFiles.Where(x=>Directory.Exists(x)));

            files.AddRange(foldersAndFiles.Where(x => File.Exists(x)));

            string iconsDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Icons");

            if (!Directory.Exists(iconsDir))
                Directory.CreateDirectory(iconsDir);

            foreach (string file in files)
            {
                IconAdder iconAdder = new IconAdder();
                iconAdder.AddIcon(file);
            }

        }

        private void AddDirItemsToList(IEnumerable<string> items)
        {
            foreach (string item in items)
            {

                files.AddRange(Directory.GetFiles(item).Where(x => File.Exists(x) && !files.Contains(x) && Path.GetExtension(x).ToLower() == ".url"));
                
                AddDirItemsToList(Directory.GetDirectories(item));
            }
        }



    }
}
